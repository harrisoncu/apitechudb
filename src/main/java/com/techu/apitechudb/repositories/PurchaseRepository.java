package com.techu.apitechudb.repositories;


import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.PurchaseModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PurchaseRepository {

    public PurchaseModel save(PurchaseModel purchase) {
        System.out.println("save en PurchaseRepository");

        ApitechudbApplication.purchaseModels.add(purchase);

        return purchase;
    }

    public List<PurchaseModel> findAll() {
        System.out.println("findAll en PurchaseRepository");

        return ApitechudbApplication.purchaseModels;
    }

    public Optional<PurchaseModel> findById(String id) {
        System.out.println("findById en PurchaseRepository");
        System.out.println("La id es " + id);

        Optional<PurchaseModel> result = Optional.empty();

        for (PurchaseModel userInList : ApitechudbApplication.purchaseModels) {
            if (userInList.getId().equals(id)) {
                System.out.println("Usuario encontrado id " + id);
                result = Optional.of(userInList);
            }
        }
        return result;
    }


}