package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
//import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public ProductService addProduct(ProductModel product) {
        System.out.println("add en ProductService");

        ProductService result = new ProductService();

        result.setProduct(product);

        if (this.userService.findById(product.getUserId()).isPresent() == false){
            System.out.println("El usuario de la compra no se ha encontrado");

            result.setMsg("El usuario de la compra no se ha encontrado");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        if (this.getById(product.getId()).isPresent() == true) {
            System.out.println("Ya hay una compra con esa id");

            result.setMsg("Ya hay una compra con esa id");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        float amount = 0;
        for (Map.Entry<String,Integer> purchaseItem : product.getId().()){
            if (this.productService.findById(purchaseItem.getKey()).isPresent() == false) {
                System.out.println("El producto con la id " + purchaseItem.getKey() +
                        "no se encuentra en el sistema");

                result.setMsg("El producto con la id " + purchaseItem.getKey() +
                        "no se encuentra en el sistema");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

                return result;
            } else {
                System.out.println("Añadiendo valor de " + purchaseItem.getValue() + "unidades del producto al total");

                amount +=
                        (this.productService.findById(purchaseItem.getKey()).get().getPrice()
                                * purchaseItem.getValue());
            }
        }

        product.setAmount(amount);
        this.productRepository.save(purchase);
        result.setMsg("Compra añadida correctamente");
        result.setResponseHttpStatusCode(HttpStatus.OK);

        return result;
    }

    public List<PurchaseModel> getPurchases() {
        System.out.println("getPurchase en PurchaseService");

        return this.productService.findAll();
    }

    public Optional<PurchaseModel> getById(String id) {
        System.out.println("getById en PurchaseService");
        System.out.println("La id es " + id);

        return this.productService.findById(id);
    }
}