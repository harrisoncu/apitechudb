package com.techu.apitechudb.models;

import java.util.Map;

public class PurchaseModel {

    private String id;
    private String userId;
    private float amount;
    private Map<String, Integer> purchaseItems;

    public PurchaseModel(String id, String userId, float amount, Map purchaseItems) {
        this.id = id;
        this.userId = userId;
        this.amount = amount;
        this.purchaseItems = purchaseItems;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public float getAmount() {
        return this.amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Map<String, Integer> getPurchaseItems() {
        return this.purchaseItems;
    }

    public void setPurchaseItems(Map<String, Integer> purchaseItems) {
        this.purchaseItems = purchaseItems;
    }
}



