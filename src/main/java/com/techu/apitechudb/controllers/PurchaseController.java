package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {

    //*Servicio ¿Donde coloca un servicio y para que se utiliza?
    //*Repositorio
    //(Criterio)
    //Conexion BD
    //Lance la consulta a la BD
    //Consulta a la BD

    @Autowired
    PurchaseServiceResponse PurchaseServiceReponse;

    @GetMapping("/purchase")
    public ResponseEntity<List<PurchaseModel>> getPurchase() {
        System.out.println("getPurchase");

        return new ResponseEntity<>(
                this.PurchaseServiceReponse.getPurchase().;
                HttpStatus.OK
        );
    }

    @PostMapping("/purchase")
    public ResponseEntity<PurchaseModel> addProduct(@RequestBody PurchaseModel product) {
        System.out.println("addPurchase");
        System.out.println("La id del producto a crear es " + product.getId());
        System.out.println("La descripción del producto a crear es " + getPurchase().toString());
        System.out.println("El precio del producto a crear es " + getPurchase().toString());

        return new ResponseEntity<>(
                this.PurchaseServiceReponse.add(product),
                HttpStatus.CREATED
        );
    }

    @GetMapping("/purchase/{id}")
    public ResponseEntity<Object> getPuchasebyId(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("La id del producto a buscar es " + id);

        Optional<PurchaseModel> result = this.PurchaseServiceReponse.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );

    }

    @PutMapping("/purchase/{id}")
    public ResponseEntity<Object> updateProduct(@RequestBody PurchaseModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("La id del producto que se va actualizar en parámetro es " + id);
        System.out.println("La id del producto que se va actualizar es " + product.getId());
        System.out.println("La descripción del producto que se va actualizar es " + product.getDesc());
        System.out.println("El precio del producto que se va actualizar es " + .getPrice());

        return new ResponseEntity<>(this.PurchaseServiceReponse.update(product), HttpStatus.OK);
    }

    @DeleteMapping("/purchase/{id}")
    public ResponseEntity<String > deleteProduct(@PathVariable String id) {
        System.out.println("delete PurchaseService");
        System.out.println("La id del producto a borrar " + id);

        boolean deleteProduct = this.PurchaseServiceReponse.delete(id);

        return new ResponseEntity<>(
                deleteProduct ? "Producto borrado" : "Producto no encontrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

}
